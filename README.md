To test:
 - docker-compose up -d
 - web will be avaiable at http://127.0.0.1:9010/
 - setUp stream (http://127.0.0.1:9010/system/inputs)
 ![graylog](./how_to/graylog.png)
 - run on localhost
 `docker run --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 busybox echo Hello Graylog`
